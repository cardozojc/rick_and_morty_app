//
//  Request.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import Foundation

/// Object: API Call
final class Request {
    
    /// API Values
    private struct Values {
        static let baseUrl = "https://rickandmortyapi.com/api"
        static let character = "character";
        static let episode = "episode";
    }
    
    /// Enpoint Required
    private let endpoint: String
    
    /// Path for API Details Request
    private let path: [String]
    
    /// Parameters for API Request
    private let parameters: [URLQueryItem]
    
    /// Build URL for API Request
    private var urlBuilded: String {
        var url = Values.baseUrl
        url += "/"
        url += endpoint
        
        if !path.isEmpty {
            path.forEach({
                url += "/\($0)"
            })
        }
        
        if !parameters.isEmpty {
            url += "?"
            let stringParameters = parameters.compactMap({
                guard let val = $0.value else { return nil }
                return "\($0.name)=\(val)"
            }).joined(separator: "&")
            
            url += stringParameters
        }
        
        return url
    }
    
    // MARK: - Public
    
    /// API Method
    public var httpMethod = "GET"
    
    /// Construct API Url
    public var url: URL? {
        return URL(string: urlBuilded)
    }
    
    /// Construct Request
    /// - Parameters:
    ///   - endpoint: API endpoint Request
    ///   - path: Path values for request
    ///   - parameters: parameters values for request
    public init(endpoint: String, path: [String] = [], parameters: [URLQueryItem] = []) {
        self.endpoint = endpoint
        self.path = path
        self.parameters = parameters
    }
    
    convenience init?(url: URL) {
        let string = url.absoluteString
        
        // Check if url is part of our domain
        if !string.contains(Values.baseUrl) {
            return nil
        }
        
        // Delete base url from url
        let extraData = string.replacingOccurrences(of: Values.baseUrl+"/", with: "")
        
        if extraData.contains("/") {
            let data = extraData.components(separatedBy: "/")
            if !data.isEmpty {
                let newEndPoint = data[0]
                var newPath: [String] = []
                
                if data.count > 1 {
                    newPath = data
                    newPath.removeFirst()
                }
                
                self.init(endpoint: data[0], path: newPath)
                return
            }
        } else if extraData.contains("?") {
            let data = extraData.components(separatedBy: "?")
            if !data.isEmpty, data.count >= 2 {
                let newEndPoint = data[0]
                let items = data[1]
                
                let requestItems: [URLQueryItem] = items.components(separatedBy: "&").compactMap({
                    guard $0.contains("=") else {
                        return nil
                    }
                    
                    let values = $0.components(separatedBy: "=")
                    
                    return URLQueryItem(name: values[0], value: values[1])
                })
                
                self.init(endpoint: Values.character, parameters: requestItems)
                return
            }
        }

        return nil
    }
}

extension Request {
    static let getAllChatacters = Request(endpoint: Values.character)
}
