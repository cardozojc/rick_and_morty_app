//
//  Service.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import Foundation

/// Servicio API
final class Service {
    
    /// Singleton Instance
    static let share = Service()
    
    /// Constructor
    private init() {}
    
    
    /// Send API Call
    /// - Parameters:
    ///   - request: request instance
    ///   - type: object type expected on response
    ///   - completion: response or error data
    public func execute<T: Codable>(
        _ request: Request,
        expecting type: T.Type,
        completion: @escaping (Result<T, Error>) -> Void)
    {
        guard let urlRequest = self.createRequest(from: request) else {
            completion(.failure(ServiceErrors.failedToCreateNewRequest))
            return
        }
        
        #if DEBUG
        print("Request --> \(request.url?.absoluteString ?? "")")
        #endif
        
        let task = URLSession.shared.dataTask(with: urlRequest) { data, _, error in
            guard let newData = data, error == nil else {
                completion(.failure(error ?? ServiceErrors.failedToGetServiceData))
                return
            }
            
            /// Decode service response
            do {
                let jsonResult = try JSONDecoder().decode(type.self, from: newData)
                completion(.success(jsonResult))
            }
            catch{
                completion(.failure(error))
            }
        }
        
        task.resume()
        
    }
    
    private func createRequest(from request: Request) -> URLRequest? {
        guard let url = request.url else { return nil }
        
        var newRequest = URLRequest(url: url)
        newRequest.httpMethod = request.httpMethod
        return newRequest
    }
}
