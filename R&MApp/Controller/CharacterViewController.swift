//
//  CharacterViewController.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import UIKit

/// Controller to show character info details
final class CharacterViewController: UIViewController {

    private let viewModel: CharacterViewViewModel
    private let characterView: CharacterView
    
    // MARK: - Init
    init(viewModel: CharacterViewViewModel) {
        self.viewModel = viewModel
        self.characterView = CharacterView(frame: .zero, viewModel: viewModel)
        super.init(nibName: nil, bundle:nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    // MARK: - View Setup
    private func setUpView() {
        self.view.backgroundColor = .systemBackground
        self.navigationItem.title = viewModel.title
        view.addSubview(characterView)
        setConstraints()
        
        characterView.collectionView?.delegate = self
        characterView.collectionView?.dataSource = self
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            characterView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            characterView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            characterView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            characterView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor)
        ])
    }
    
    private func didTapSearch() {
        let viewController = SearchViewController(config: .init(searchType: .character))
        viewController.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension CharacterViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.characterSections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sectionType = viewModel.characterSections[section]
        switch sectionType {
        case .photo:
            return 1
        case .information(let viewModel):
            return viewModel.count
        case .location(let viewModel):
            return viewModel.count
        case .episodes(let viewModel):
            return viewModel.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let sectionType = viewModel.characterSections[indexPath.section]
        switch sectionType {
        case .photo(let viewModel):
            guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: ImageCollectionViewCell.cellId,
                for: indexPath
            ) as? ImageCollectionViewCell else {
                fatalError()
            }
            
            cell.configure(with: viewModel)
            return cell
            
        case .information(let viewModels):
            guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: InfoCollectionViewCell.cellId,
                for: indexPath
            ) as? InfoCollectionViewCell else {
                fatalError()
            }
            
            cell.configure(with: viewModels[indexPath.row])
            return cell
            
        case .location(let viewModels):
            guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: InfoCollectionViewCell.cellId,
                for: indexPath
            ) as? InfoCollectionViewCell else {
                fatalError()
            }
            
            cell.configure(with: viewModels[indexPath.row])
            return cell
            
        case .episodes(let viewModels):
            guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: EpisodeCollectionViewCell.cellId,
                for: indexPath
            ) as? EpisodeCollectionViewCell else {
                fatalError()
            }
            
            cell.configure(with: viewModels[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        let sectionType = viewModel.characterSections[indexPath.section]
        
        switch sectionType {

        case .location(let viewModels):
            let iteamAction = viewModels[indexPath.item].getAction
            
            if iteamAction == "map" {
                let newVC = MapViewController()
                navigationController?.pushViewController(newVC, animated: true)
                return true
            }
            
            return false
            
        default:
            return false
        }
    }
}
