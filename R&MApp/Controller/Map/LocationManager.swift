//
//  LocationManager.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 04/10/2023.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    static let shared = LocationManager()
    
    public func findLocations(with query: String, completion: @escaping (([MapLocation]) -> Void)) {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(query) { places, error in
            guard let places = places, error == nil else {
                completion([])
                return
            }
            
            let mapLocations: [MapLocation] = places.compactMap({ place in
                var locationName = ""
                
                if let name = place.name {
                    locationName += name
                }
                
                if let area = place.administrativeArea {
                    locationName += ", \(area)"
                }
                
                if let locality = place.locality {
                    locationName += ", \(locality)"
                }
                
                if let country = place.country {
                    locationName += ", \(country)"
                }
                
                print("\n\(place)\n\n")
                
                let result = MapLocation(title: locationName, coordinates: place.location?.coordinate)
                return result
            })
            
            completion(mapLocations)
        }
    }
}
