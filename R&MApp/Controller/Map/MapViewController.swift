//
//  MapViewController.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 03/10/2023.
//

import UIKit
import CoreLocation
import FloatingPanel
import MapKit
import Goo

class MapViewController: UIViewController, CLLocationManagerDelegate, MapSearchViewControllerDelegate {
    
    let manager = CLLocationManager()
    let mapView = MKMapView()
    let panel = FloatingPanelController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    // MARK: - View Setup
    private func setUpView() {
        self.view.backgroundColor = .systemBackground
        self.navigationItem.title = "Location"
        navigationController?.navigationBar.tintColor = .label
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        mapView.delegate = self
        
        view.addSubview(mapView)
        
        let searchVC = MapSearchViewController()
        searchVC.delegate = self
        
        panel.set(contentViewController: searchVC)
        panel.addPanel(toParent: self)
        panel.move(to: .tip, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mapView.frame = view.bounds
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        manager.stopUpdatingLocation()
        mapView.setRegion(.userRegion, animated: true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = .userLocation
        mapView.addAnnotation(pin)
    }
    
    func mapSearchViewController(_ vc: MapSearchViewController, didSelectLocationWith coordinates: CLLocationCoordinate2D?) {
        
        if var annotations = mapView.annotations as [MKAnnotation]? {
            mapView.removeAnnotations(annotations)
        }
        
        mapView.removeOverlays(mapView.overlays)
        
        panel.move(to: .tip, animated: true)
        
        guard let coordinates = coordinates else { return }
        
        let pinDestination = MKPointAnnotation()
        pinDestination.coordinate = coordinates
        
        let pinStart = MKPointAnnotation()
        pinStart.coordinate = .userLocation
        
        mapView.addAnnotation(pinDestination)
        mapView.addAnnotation(pinStart)
        
        let startLocation = MKPlacemark(coordinate: .userLocation)
        let destionation = MKPlacemark(coordinate: coordinates)
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: startLocation)
        request.destination = MKMapItem(placemark: destionation)
        request.transportType = .automobile
        request.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] (response, error) in
            guard let response = response else { return }
            
            for route in response.routes {
                mapView.addOverlay(route.polyline)
                let edgeInsets = UIEdgeInsets(top: 10.0, left: 15.0, bottom: 10.0, right: 15.0)
                mapView.setVisibleMapRect(route.polyline.boundingMapRect, edgePadding: edgeInsets, animated: true)
            }
        }
    }
}

extension CLLocationCoordinate2D {
    static var userLocation: CLLocationCoordinate2D {
        return .init(latitude: -34.6037389, longitude: -58.3830718)
    }
}

extension MKCoordinateRegion {
    static var userRegion: MKCoordinateRegion {
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        return .init(center: .userLocation, span: span)
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .systemBlue
        return render
    }
}
