//
//  FilterSelectorViewController.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 04/08/2023.
//

import UIKit

final class FilterSelectorViewController: UIViewController {

    private let filter: InputViewViewModel.FilterOptions
    private let filterSelected: ((String) -> Void)
    
    private let tableViewFilter: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    init(filter: InputViewViewModel.FilterOptions, filterSelected: @escaping(String) -> Void) {
        self.filter = filter
        self.filterSelected = filterSelected
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    private func setUpView() {
        view.backgroundColor = .secondarySystemBackground
        setUpTableView()
    }
    
    private func setUpTableView() {
        view.addSubview(tableViewFilter)
        tableViewFilter.delegate = self
        tableViewFilter.dataSource = self
        setConstraints()
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            tableViewFilter.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableViewFilter.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            tableViewFilter.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            tableViewFilter.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
}

extension FilterSelectorViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filter.choices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = filter.choices[indexPath.row].capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.filterSelected(filter.choices[indexPath.row])
        dismiss(animated: true)
    }
    
}
