//
//  SearchViewController.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 02/08/2023.
//

import UIKit

/// Controller for Search View
final class SearchViewController: UIViewController {
    
    struct Config {
        enum SearchType {
            case character
        }
        
        let searchType: SearchType
    }
    
    private let viewModel: SearchViewViewModel
    private let searchView: SearchView
    
    init(config: Config) {
        let viewModel = SearchViewViewModel(config: config)
        self.viewModel = viewModel
        self.searchView = SearchView(frame: .zero, viewModel: viewModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    private func setUpView() {
        title = "Search Characters"
        view.backgroundColor = .systemBackground
        view.addSubview(searchView)
        setConstraints()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Search",
                                                      style: .done,
                                                      target: self,
                                                      action: #selector(doSearchAction))
        
        searchView.delegate = self
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            searchView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            searchView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            searchView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    @objc
    private func doSearchAction() {
        viewModel.searchData()
    }
    
}

extension SearchViewController: SearchViewDelegate {
    func searchView(_ searchView: SearchView, didSelectFilter filter: InputViewViewModel.FilterOptions) {
        let filterSelectorVC = FilterSelectorViewController(filter: filter) { [weak self] filterChoice in
            #if DEBUG
            print("Fliter Choice -> \(filterChoice)")
            #endif

            DispatchQueue.main.async {
                self?.viewModel.setFilterSelected(value: filterChoice, for: filter)
            }
        }
        filterSelectorVC.sheetPresentationController?.detents = [.medium()]
        filterSelectorVC.sheetPresentationController?.prefersGrabberVisible = true
        present(filterSelectorVC, animated: true)
    }
    
    func searchView(_ searchView: SearchView, didSelectCharacter character: Character) {
        let vc = CharacterViewController(viewModel: .init(character: character))
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
}
