//
//  ViewController.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import UIKit

/// Controller to show and search character
final class ViewController: UIViewController {

    private let charactersListView = CharactersListView()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    // MARK: - View Setup
    private func setUpView() {
        charactersListView.delegate = self
        self.view.backgroundColor = .systemBackground
        self.navigationItem.title = "Rick and Morty"
        navigationController?.navigationBar.tintColor = .label
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        view.addSubview(charactersListView)
        setSearchBtn()
        setConstraints()
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            charactersListView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            charactersListView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            charactersListView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            charactersListView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor)
        ])
    }
    
    private func setSearchBtn() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search,
                                                            target: self,
                                                            action: #selector(tabShare))
        
        navigationItem.rightBarButtonItem?.tintColor = .label
    }
    
    @objc
    private func tabShare() {
        let viewController = SearchViewController(config: SearchViewController.Config(searchType: .character))
        viewController.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ViewController: CharactersListViewDelegate {
    func charactersListView(_ charactersListView: CharactersListView, showCharacter character: Character) {
        let viewModel = CharacterViewViewModel(character: character)
        let characterVC = CharacterViewController(viewModel: viewModel)
        navigationController?.pushViewController(characterVC, animated: true)
    }
}

