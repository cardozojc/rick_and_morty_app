//
//  AllCharacterResponse.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import Foundation

struct AllCharacterResponse: Codable {
    let info: Info
    let results: [Character]
}

struct Info: Codable {
    let count: Int
    let pages: Int
    let next: String?
    let prev: String?
}
