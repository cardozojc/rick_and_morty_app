//
//  Character.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import Foundation

/// Model: Character
struct Character: Codable {
    let id: Int
    let name: String
    let status: Status
    let species: String
    let type: String
    let gender: Gender
    let origin: Origen
    let location: Location
    let image: String
    let episode: [String]
    let url: String
    let created: String
}


