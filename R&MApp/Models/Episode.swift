//
//  Episode.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 02/08/2023.
//

import Foundation

struct Episode: Codable, EpisodeDataToRender {
    let id: Int
    let name: String
    let air_date: String
    let episode: String
    let characters: [String]
    let url: String
    let created: String
}
