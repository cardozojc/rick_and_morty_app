//
//  Gender.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import Foundation

/// Enum: Gender for character
enum Gender: String, Codable {
    case female = "Female"
    case male = "Male"
    case genderless = "Genderless"
    case `unknown` = "unknown"
}
