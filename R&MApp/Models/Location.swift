//
//  Location.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import Foundation

/// Model: Location
struct Location: Codable {
    let name: String
    let url: String
}
