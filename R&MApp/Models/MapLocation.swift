//
//  MapLocation.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 04/10/2023.
//

import Foundation
import CoreLocation

/// Model: Location
struct MapLocation {
    let title: String
    let coordinates: CLLocationCoordinate2D?
}

