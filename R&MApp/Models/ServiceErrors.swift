//
//  ServiceErrors.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import Foundation

/// Enum: Gender for character
enum ServiceErrors: Error {
    case failedToCreateNewRequest
    case failedToGetServiceData
}
