//
//  ImageHandler.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 31/07/2023.
//

import Foundation

final class ImageHandler {
    
    /// Shared Instance
    static let shared = ImageHandler()
    
    /// Cache Memory
    private var imageCache = NSCache<NSString, NSData>()
    
    private init() {}
    
    /// Download image for server
    /// - Parameters:
    ///   - url: Source url
    ///   - completion: Callback
    public func downloadImage(_ url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        let key = url.absoluteString as NSString
        if let data = imageCache.object(forKey: key) {
            #if DEBUG
            print("Get Image for Cache --> \(key)")
            #endif
            completion(.success(data as Data))
            return
        }

        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { [weak self] data, _, error in
            guard let data = data, error == nil else {
                completion(.failure(error ?? URLError(.badServerResponse)))
                return
            }
            let value = data as NSData
            self?.imageCache.setObject(value, forKey: key)
            #if DEBUG
            print("Get Image for Service --> \(key)")
            #endif
            completion(.success(data))
        }
        task.resume()
    }
}
