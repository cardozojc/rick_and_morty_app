//
//  CharacterViewViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 30/07/2023.
//

import UIKit

final class CharacterViewViewModel {

    private let character: Character
    
    enum CharacterSections {
        case photo(viewModel: ImageCollectionViewCellViewModel)
        case information(viewModels: [InfoCollectionViewCellViewModel])
        case location(viewModels: [InfoCollectionViewCellViewModel])
        case episodes(viewModels: [EpisodeCollectionViewCellViewModel])
    }
    
    public var characterSections: [CharacterSections] = []
    
    private var characterURL: URL? {
        return URL(string: character.url)
    }
    
    // MARK: - Init
    init(character: Character) {
        self.character = character
        startSections()
    }
    
    private func startSections() {
        characterSections = [
            .photo(viewModel: .init(image: URL(string: character.image))),
            .information(viewModels: [
                .init(type: .status, info: character.status.rawValue),
                .init(type: .species, info: character.species),
                .init(type: .gender, info: character.gender.rawValue),
                .init(type: .created, info: character.created),
            ]),
            .location(viewModels: [
                .init(type: .origin, info: character.origin.name),
                .init(type: .location, info: character.location.name),
            ]),
            .episodes(viewModels: character.episode.compactMap ({
                return EpisodeCollectionViewCellViewModel(episode: URL(string: $0))
            }))
        ]
    }
    
    public var title: String {
        character.name.capitalized
    }
    
    // MARK: - Layouts
    public func createPhotoSectionLayout() -> NSCollectionLayoutSection {
        
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0)
            )
        )
        
        item.contentInsets = NSDirectionalEdgeInsets(top: 5, leading: 10, bottom: 10, trailing: 10)

        let group = NSCollectionLayoutGroup.vertical(
            layoutSize:  NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(0.45)
            ),
            subitems: [item]
        )
        
        let section = NSCollectionLayoutSection(group: group)
        return section
    }

    public func createInfoSectionLayout() -> NSCollectionLayoutSection {
        
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0)
            )
        )
        
        item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 10, bottom: 2, trailing: 10)

        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize:  NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .absolute(50)
            ),
            subitems: isPhone() ? [item, item] : [item, item, item, item]
        )
        let section = NSCollectionLayoutSection(group: group)
        return section
    }

    public func createEpisodeSectionLayout() -> NSCollectionLayoutSection {
        
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0)
            )
        )
        
        item.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10)

        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize:  NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(isPhone() ? 0.8 : 0.4),
                heightDimension: .absolute(150)
            ),
            subitems: [item]
        )
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .groupPaging
        return section
    }
    
    private func isPhone() -> Bool {
        UIDevice.current.userInterfaceIdiom == .phone ? true : false
    }

}
