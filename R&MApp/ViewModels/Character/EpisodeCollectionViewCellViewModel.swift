//
//  EpisodeCollectionViewCellViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 01/08/2023.
//

import Foundation

protocol EpisodeDataToRender {
    var name: String { get }
    var air_date: String { get }
    var episode: String { get }
}

final class EpisodeCollectionViewCellViewModel {
    
    private let episode: URL?
    private var downloading = false
    private var episodeDataToRender: ((EpisodeDataToRender) -> Void)?
    
    private var episodeData: Episode? {
        didSet {
            guard let data = episodeData else {
                return
            }
            episodeDataToRender?(data)
        }
    }
    
    init(episode: URL?) {
        self.episode = episode
    }
    
    public func registerForData(_ completion: @escaping (EpisodeDataToRender) -> Void) {
        self.episodeDataToRender = completion
    }
    
    public func getEpisode() {
        guard let url = episode,
            let request = Request(url: url) else {
            return
        }
        
        Service.share.execute(request, expecting: Episode.self) { [weak self] result in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    self?.episodeData = data
                }
            case .failure(let feilure):
                #if DEBUG
                print(String(describing: feilure))
                #endif
                break
            }
        }
    }
}
