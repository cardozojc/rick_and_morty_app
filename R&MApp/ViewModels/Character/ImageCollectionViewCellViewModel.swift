//
//  ImageCollectionViewCellViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 01/08/2023.
//

import Foundation

final class ImageCollectionViewCellViewModel {

    private let image: URL?
    
    init(image: URL?) {
        self.image = image
    }
    
    public func getImage(completion: @escaping (Result<Data, Error>) -> Void) {
        guard let url = image else {
            completion(.failure(URLError(.fileDoesNotExist)))
            return
        }
        ImageHandler.shared.downloadImage(url, completion: completion)
    }
}
