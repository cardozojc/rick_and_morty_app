//
//  InfoCollectionViewCellViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 01/08/2023.
//

import UIKit

final class InfoCollectionViewCellViewModel {
    
    private let type: InfoTypes
    public let info: String
    
    public var getTitle: String {
        return type.getTitle
    }
    
    public var getInfo: String {
        if info.isEmpty { return "Unknown" }
        
        if type == .created {
            if let date = Self.dateFormatter.date(from: info) {
                return Self.newDateFormatter.string(from: date)
            }
        }
        
        return info
    }
    
    public var getIcon: UIImage? {
        return type.getIcon
    }
    
    public var getColor: UIColor {
        switch type {
        case .status:
            return info == "Alive" ? .systemGreen : info == "Dead" ? .systemRed : .label
        case .species:
            return .systemYellow
        case .gender:
            return .systemPurple
        case .origin:
            return .systemPink
        case .location:
            return .systemMint
        case .created:
            return .systemBlue
        }
    }
    
    public var getAction: String {
        switch type {
        case .status:
            return "none"
        case .species:
            return "none"
        case .gender:
            return "none"
        case .origin:
            return "none"
        case .location:
            return "map"
        case .created:
            return "none"
        }
    }
    
    enum InfoTypes {
        case status
        case species
        case gender
        case origin
        case location
        case created
        
        var getIcon: UIImage? {
            switch self {
            case .status:
                return UIImage(systemName: "heart.circle")
            case .species:
                return UIImage(systemName: "square.on.square.badge.person.crop.fill")
            case .gender:
                return UIImage(systemName: "person.circle")
            case .origin:
                return UIImage(systemName: "mappin.circle")
            case .location:
                return UIImage(systemName: "map.circle")
            case .created:
                return UIImage(systemName: "calendar.circle")
            }
        }
        
        var getTitle: String {
            switch self {
            case .status:
                return "Status"
            case .species:
                return "Species"
            case .gender:
                return "Gender"
            case .origin:
                return "Origin"
            case .location:
                return "Location"
            case .created:
                return "Created"
            }
        }
    }
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSZ"
        formatter.timeZone = .current
        return formatter
    }()
    
    static let newDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        formatter.timeZone = .current
        return formatter
    }()
    
    init(type: InfoTypes, info: String) {
        self.type = type
        self.info = info
    }
}
