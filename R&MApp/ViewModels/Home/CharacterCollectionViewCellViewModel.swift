//
//  CharacterCollectionViewCellViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 30/07/2023.
//

import Foundation

final class CharacterCollectionViewCellViewModel: Hashable, Equatable{
    
    public let imgUrl: URL?
    public let name: String
    
    static func == (lhs: CharacterCollectionViewCellViewModel, rhs: CharacterCollectionViewCellViewModel) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(imgUrl)
        hasher.combine(name)
    }
    
    init(imgUrl: URL?, name: String) {
        self.imgUrl = imgUrl
        self.name = name
    }
    
    public func loadImage(completion: @escaping (Result<Data, Error>) -> Void) {
        guard let url = imgUrl else {
            completion(.failure(URLError(.fileDoesNotExist)))
            return
        }
        ImageHandler.shared.downloadImage(url, completion: completion)
    }
}
