//
//  CharactersListViewViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import UIKit

protocol CharactersListViewViewModelDelegate: AnyObject {
    func loadedCharacters()
    func LoadedMoreCharacters(with indexPaths: [IndexPath])
    func showCharacter(_ character: Character)
}

final class CharactersListViewViewModel: NSObject {
    
    public weak var delegate: CharactersListViewViewModelDelegate?
    private var loadingMoreCharacters = false
    
    private var characters: [Character] = [] {
        didSet {
            for character in characters {
                let newCharacter = CharacterCollectionViewCellViewModel(
                    imgUrl: URL(string: character.image),
                    name: character.name
                )
                if !charactersCells.contains(newCharacter) {
                    charactersCells.append(newCharacter)
                }
            }
        }
    }
    
    private var charactersCells: [CharacterCollectionViewCellViewModel] = []
    private var info: Info? = nil
    
    /// Get initials charactes from API services and set data
    public func getCharacters() {
        Service.share.execute(.getAllChatacters, expecting: AllCharacterResponse.self) { [weak self] result in
            switch result {
            case .success(let data):
                let responseResults = data.results
                let responseInfo = data.info
                self?.characters = responseResults
                self?.info = responseInfo
                DispatchQueue.main.async {
                    self?.delegate?.loadedCharacters()
                }
            case .failure(let error):
                #if DEBUG
                print(String(describing: error))
                #endif
                break
            }
        }
    }
    
    /// Get more characters from API service and set data
    public func getMoreCharacters(url: URL) {
        guard !loadingMoreCharacters else {
            return
        }
        
        loadingMoreCharacters = true
        guard let newRequest = Request(url: url) else {
            loadingMoreCharacters = false
            return
        }
        
        Service.share.execute(newRequest, expecting: AllCharacterResponse.self) { [weak self] result in
            guard let confirmSelf = self else {
                return
            }
            
            switch result {
            case .success(let data):
                let newResults = data.results
                let newInfo = data.info
                confirmSelf.info = newInfo
                
                /// Get de total numbers of character after get new page
                let charactersCounts = confirmSelf.characters.count
                let newCharactersCount = newResults.count
                let totalCharactersCount = charactersCounts + newCharactersCount
                
                /// Set indexpath array for load more characters
                /// Initial Index, ( - 1 because the array starts at index 0 )
                let initialIntex  = totalCharactersCount - newCharactersCount
                let newCharacters: [IndexPath] = Array(initialIntex..<(initialIntex + newCharactersCount)).compactMap({
                    return IndexPath(row: $0, section: 0)
                })
                
                confirmSelf.characters.append(contentsOf: newResults)
                
                #if DEBUG
                print("Total New Characters --> \(String(confirmSelf.characters.count))")
                #endif
                
                DispatchQueue.main.async {
                    confirmSelf.delegate?.LoadedMoreCharacters(with: newCharacters)
                    confirmSelf.loadingMoreCharacters = false
                }
            case .failure(let error):
                confirmSelf.loadingMoreCharacters = false
                #if DEBUG
                print(String(describing: error))
                #endif
                break
            }
        }
    }
    
    /// Controller if we have more data to load
    public var canLoadMoreData: Bool {
        return info?.next != nil
    }
    
}

extension CharactersListViewViewModel: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return charactersCells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CharacterCollectionViewCell.cellId, for: indexPath) as? CharacterCollectionViewCell else {
            fatalError("Error loading cell")
        }
        
        let cellViewModel = charactersCells[indexPath.row]
        cell.setCollectionViewCell(with: cellViewModel)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard kind == UICollectionView.elementKindSectionFooter,
              let footer = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: LoadMoreFooterCollectionReusableView.id,
                for: indexPath
              ) as? LoadMoreFooterCollectionReusableView else {
            fatalError("Unsupported")
        }
        
        footer.startSpinner()
        return footer
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        guard canLoadMoreData else {
            return .zero
        }
        return CGSize(width: collectionView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewBounds = UIScreen.main.bounds
        let width = (viewBounds.width - 35) / 2
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let characterSelected = characters[indexPath.row]
        delegate?.showCharacter(characterSelected)
    }
    
}

extension CharactersListViewViewModel: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard canLoadMoreData,
            !loadingMoreCharacters,
            !charactersCells.isEmpty,
            let nextPage = info?.next,
            let nextURL = URL(string: nextPage) else {
            return
        }
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] t in
            let offset = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height
            let scrollHeight = scrollView.frame.size.height
            
            if offset >= (contentHeight - scrollHeight - 100) {
                self?.getMoreCharacters(url: nextURL)
            }
            
            t.invalidate()
        }
    }
    
}
