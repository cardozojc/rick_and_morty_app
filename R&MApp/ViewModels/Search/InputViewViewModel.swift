//
//  InputViewViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 03/08/2023.
//

import Foundation

final class InputViewViewModel {
    
    enum FilterOptions: String {
        case status = "Status"
        case gender = "Gender"
        case species = "Species"
        
        var likeParamValue: String {
            switch self {
            case .status: return "status"
            case .gender: return "gender"
            case .species: return "species"
            }
        }
        
        var choices: [String] {
            switch self {
            case .status:
                return ["alive", "dead", "unknown"]

            case .gender:
                return ["male", "female", "genderless", "unknown"]

            case .species:
                return ["human", "humanoid", "alien", "poopybutthole", "animal", "mythological creature", "robot", "crononberg", "disease", "alphabetrian"]
            }
        }
    }
    
    let filters: [FilterOptions] = [.status, .gender, .species]
}
