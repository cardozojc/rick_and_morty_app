//
//  NoResultsViewViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 03/08/2023.
//

import UIKit

final class NoResultsViewViewModel {
    
    let title = "No Results"
    let icon = UIImage(systemName: "magnifyingglass.circle")
    
}
