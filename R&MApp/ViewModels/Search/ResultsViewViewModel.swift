//
//  ResultsViewViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 08/08/2023.
//

import Foundation

final class ResultsViewViewModel {
    
    public private(set) var results: [CharacterCollectionViewCellViewModel]
    
    private var next: String?
    private var allSearchResult: [Character]?
    
    init(results: [CharacterCollectionViewCellViewModel], next: String?, allSearchResult: [Character]?) {
        self.results = results
        self.next = next
        self.allSearchResult = allSearchResult
    }
    
    public private(set) var isLoadingMoreResults = false
    
    public var shouldShowLoadMoreIndicator: Bool {
        return next != nil
    }
    
    public func getMoreResults(completion: @escaping ([any Hashable]) -> Void) {
        guard !isLoadingMoreResults else {
            return
        }
        
        guard let nextUrlString = next,
              let url = URL(string: nextUrlString) else {
            return
        }
        
        isLoadingMoreResults = true
        
        guard let request = Request(url: url) else {
            isLoadingMoreResults = false
            return
        }
        
        
        if !results.isEmpty {
            let existingResults = results
            
            Service.share.execute(request, expecting: AllCharacterResponse.self) { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .success(let responseModel):
                    let moreResults = responseModel.results
                    self?.allSearchResult?.append(contentsOf: moreResults)
                    let info = responseModel.info
                    strongSelf.next = info.next // Capture new pagination url
                    
                    let additionalResults = moreResults.compactMap({
                        return CharacterCollectionViewCellViewModel(imgUrl: URL(string: $0.image), name: $0.name)
                    })
                    var newResults: [CharacterCollectionViewCellViewModel] = []
                    newResults = existingResults + additionalResults
                    strongSelf.results = [CharacterCollectionViewCellViewModel](newResults)
                    
                    DispatchQueue.main.async {
                        strongSelf.isLoadingMoreResults = false
                        
                        // Notify via callback
                        completion(newResults)
                    }
                case .failure(let failure):
                    #if DEBUG
                    print(String(describing: failure))
                    #endif
                    self?.isLoadingMoreResults = false
                    break
                }
            }
            
        }
    }
    
    public func getSelectedCharacter(at index: Int) -> Character? {
        guard let allCharacters = allSearchResult as? [Character] else {
            return nil
        }
        return allCharacters[index]
    }
}
