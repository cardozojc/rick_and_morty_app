//
//  SearchViewViewModel.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 03/08/2023.
//

import UIKit

final class SearchViewViewModel {
    
    private let config: SearchViewController.Config
    
    private var filterMap: [InputViewViewModel.FilterOptions: String] = [:]
    private var searchText = ""

    private var filterMapSelectedData: (((InputViewViewModel.FilterOptions, String)) -> Void)?
    private var searchResultModel: Codable?
    private var searchResultHandler: ((ResultsViewViewModel) -> Void)?
    private var noResultsHandler: (() -> Void)?
    
    init(config: SearchViewController.Config) {
        self.config = config
    }
    
    public func showSearchResults(_ resultData: @escaping (ResultsViewViewModel) -> Void) {
        self.searchResultHandler = resultData
    }
    
    public func showNoResults(_ noData: @escaping () -> Void) {
        self.noResultsHandler = noData
    }
    
    public func searchData() {
        guard !searchText.trimmingCharacters(in: .whitespaces).isEmpty else {
            return
        }

        // Build arguments
        var queryParams: [URLQueryItem] = [
            URLQueryItem(name: "name", value: searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
        ]
        
        /// Set Params
        queryParams.append(contentsOf: filterMap.enumerated().compactMap({ _, data in
            let key: InputViewViewModel.FilterOptions = data.key
            let value: String = data.value
            return URLQueryItem(name: key.rawValue.lowercased(), value: value)
        }))

        /// Create Request
        let request = Request(
            endpoint: "character",
            parameters: queryParams
        )
        
        /// Send Request to API
        Service.share.execute(request, expecting: AllCharacterResponse.self) { [weak self] result in
            // Notify view of results, no results, or error
            switch result {
            case .success(let model):
                var resultsVM: ([CharacterCollectionViewCellViewModel])
                var nextUrl: String?
                if let characterResults = model as? AllCharacterResponse {
                    resultsVM = characterResults.results.compactMap({
                        return CharacterCollectionViewCellViewModel(imgUrl: URL(string: $0.image), name: $0.name)
                    })
                    nextUrl = characterResults.info.next
                    
                    self?.searchResultModel = model
                        
                    #if DEBUG
                    print("Total New Found Characters --> \(String(resultsVM.count))")
                    #endif
                    
                    let vm = ResultsViewViewModel(results: resultsVM, next: nextUrl, allSearchResult: characterResults.results)
                    self?.searchResultHandler?(vm)
                        
                }
            case .failure:
                self?.handleNoResults()
                break
            }
        }
    }
    
    private func handleNoResults() {
        noResultsHandler?()
    }
    
    public func setInputSearchText(query text: String) {
        self.searchText = text
    }
    
    public func setFilterSelected(value: String, for filter: InputViewViewModel.FilterOptions) {
        filterMap[filter] = value
        let data = (filter, value)
        filterMapSelectedData?(data)
    }
    
    public func detectedFilterChange(_ data: @escaping ((InputViewViewModel.FilterOptions, String)) -> Void) {
        self.filterMapSelectedData = data
    }
}
