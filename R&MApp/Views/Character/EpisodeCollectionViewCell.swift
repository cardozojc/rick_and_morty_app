//
//  EpisodeCollectionViewCell.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 01/08/2023.
//

import UIKit

class EpisodeCollectionViewCell: UICollectionViewCell {
    
    static let cellId = "EpisodeCollectionViewCell"
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 18, weight: .bold)
        label.textColor = .systemBackground
        return label
    }()
    
    private let episodeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16, weight: .semibold)
        label.textColor = .systemBackground
        return label
    }()
    
    private let airDateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16, weight: .regular)
        label.textColor = .systemBackground
        return label
    }()
    
    private let imgView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "randmwp")
        return view
    }()
    
    private let infoContent: UIView = {
        let view = UIView()
        view.backgroundColor = .label.withAlphaComponent(0.6)
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        episodeLabel.text = nil
        airDateLabel.text = nil
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpContentView()
    }
    
    public func configure(with viewModel: EpisodeCollectionViewCellViewModel) {
        viewModel.registerForData { [weak self] data in
            self?.titleLabel.text = data.name
            self?.episodeLabel.text = "Episode \(data.episode)"
            self?.airDateLabel.text = "Aired on \(data.air_date)"
        }
        viewModel.getEpisode()
    }
    
    private func setUpView() {
        infoContent.addSubview(titleLabel)
        infoContent.addSubview(episodeLabel)
        infoContent.addSubview(airDateLabel)
        contentView.addSubview(imgView)
        contentView.addSubview(infoContent)
        setUpContentView()
        setConstraints()
    }
    
    private func setUpContentView() {
        contentView.backgroundColor = .label.withAlphaComponent(0.4)
        contentView.layer.cornerRadius = 8
        contentView.layer.borderWidth = 2
        contentView.layer.borderColor = UIColor.label.cgColor
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: infoContent.topAnchor),
            titleLabel.leftAnchor.constraint(equalTo: infoContent.leftAnchor, constant: 10),
            titleLabel.rightAnchor.constraint(equalTo: infoContent.rightAnchor, constant: -10),
            titleLabel.heightAnchor.constraint(equalTo: infoContent.heightAnchor, multiplier: 0.4),
            
            episodeLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            episodeLabel.leftAnchor.constraint(equalTo: infoContent.leftAnchor, constant: 10),
            episodeLabel.rightAnchor.constraint(equalTo: infoContent.rightAnchor, constant: -10),
            episodeLabel.heightAnchor.constraint(equalTo: infoContent.heightAnchor, multiplier: 0.3),
            
            airDateLabel.topAnchor.constraint(equalTo: episodeLabel.bottomAnchor),
            airDateLabel.leftAnchor.constraint(equalTo: infoContent.leftAnchor, constant: 10),
            airDateLabel.rightAnchor.constraint(equalTo: infoContent.rightAnchor, constant: -10),
            airDateLabel.heightAnchor.constraint(equalTo: infoContent.heightAnchor, multiplier: 0.3),
            
            imgView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imgView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            imgView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            imgView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            infoContent.topAnchor.constraint(equalTo: contentView.topAnchor),
            infoContent.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            infoContent.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            infoContent.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
}
