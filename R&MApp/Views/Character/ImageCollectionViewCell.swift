//
//  ImageCollectionViewCell.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 01/08/2023.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    static let cellId = "ImageCollectionViewCell"
    
    private let imgContent: UIView = {
        let view = UIView()
        view.backgroundColor = .systemOrange.withAlphaComponent(0.6)
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let imgView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgView.image = nil
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpImageContenView()
    }
    
    public func configure(with viewModel: ImageCollectionViewCellViewModel) {
        viewModel.getImage { [weak self] result in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    let img = UIImage(data: data)
                    self?.imgView.image = img
                }
            case .failure:
                break
            }
        }
    }
    
    private func setUpView() {
        imgContent.addSubview(imgView)
        contentView.addSubview(imgContent)
        setConstraints()
        setUpImageContenView()
    }
    
    private func setUpImageContenView() {
        imgContent.layer.borderWidth = 4
        imgContent.layer.borderColor = UIColor.label.withAlphaComponent(0.6).cgColor
        imgContent.layer.cornerRadius = 15
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            imgContent.topAnchor.constraint(equalTo: contentView.topAnchor),
            imgContent.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            imgContent.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            imgContent.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            imgView.topAnchor.constraint(equalTo: imgContent.topAnchor),
            imgView.leftAnchor.constraint(equalTo: imgContent.leftAnchor),
            imgView.rightAnchor.constraint(equalTo: imgContent.rightAnchor),
            imgView.bottomAnchor.constraint(equalTo: imgContent.bottomAnchor),
        ])
    }
}
