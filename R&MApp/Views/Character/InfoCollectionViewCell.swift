//
//  InfoCollectionViewCell.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 01/08/2023.
//

import UIKit

class InfoCollectionViewCell: UICollectionViewCell {
    
    static let cellId = "InfoCollectionViewCell"
    private var itemAction: String = ""

    private let infoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16, weight: .bold)
        return label
    }()

    private let iconView: UIImageView = {
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.contentMode = .scaleAspectFit
        return icon
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        infoLabel.text = nil
        iconView.image = nil
        iconView.tintColor = .label
        infoLabel.textColor = .label
    }
    
    public func configure(with viewModel: InfoCollectionViewCellViewModel) {
        setInfoValue(str1: "\(viewModel.getTitle): ", str2: viewModel.getInfo)
        self.itemAction = viewModel.getAction
        iconView.image = viewModel.getIcon
        iconView.tintColor = viewModel.getColor
    }
    
    public func getItemAction() -> String {
        return self.itemAction
    }
    
    private func setUpView() {
        contentView.addSubview(iconView)
        contentView.addSubview(infoLabel)
        setInfoValue()
        setConstraints()
    }
    
    private func setInfoValue(str1: String = "", str2: String = "") {
        let part1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.label]
        let part2 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.label]

        let finalString = NSMutableAttributedString(string:str1, attributes:part1)
        let secondStyleString = NSMutableAttributedString(string:str2, attributes:part2)

        finalString.append(secondStyleString)
        self.infoLabel.attributedText = finalString
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            iconView.widthAnchor.constraint(equalToConstant: 30),
            iconView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
            iconView.topAnchor.constraint(equalTo: contentView.topAnchor),
            iconView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            infoLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: 10),
            infoLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            infoLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            infoLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
}
