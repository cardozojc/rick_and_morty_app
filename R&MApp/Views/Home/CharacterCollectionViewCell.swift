//
//  CharacterCollectionViewCell.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 30/07/2023.
//

import UIKit

/// Cell for show a character
final class CharacterCollectionViewCell: UICollectionViewCell {
    
    static let cellId = "CharacterViewCell"
    
    private let imgView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let infoView: UIView = {
        let view = UIView()
        view.backgroundColor = .secondarySystemBackground.withAlphaComponent(0.6)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = .systemFont(ofSize: 14, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgView.image = nil
        nameLabel.text = nil
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpContenView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    private func setUpView() {
        contentView.backgroundColor = .secondarySystemBackground
        infoView.addSubview(nameLabel)
        contentView.addSubview(imgView)
        contentView.addSubview(infoView)
        setConstraints()
        setUpContenView()
    }
    
    private func setUpContenView() {
        contentView.layer.shadowColor = UIColor.label.cgColor
        contentView.layer.shadowOffset = CGSize(width: 4, height: 4)
        contentView.layer.shadowOpacity = 0.4
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            imgView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imgView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            imgView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            imgView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            infoView.heightAnchor.constraint(equalToConstant: 30),
            infoView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            infoView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            infoView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            nameLabel.topAnchor.constraint(equalTo: infoView.topAnchor),
            nameLabel.leftAnchor.constraint(equalTo: infoView.leftAnchor, constant: 5),
            nameLabel.rightAnchor.constraint(equalTo: infoView.rightAnchor, constant: -5),
            nameLabel.bottomAnchor.constraint(equalTo: infoView.bottomAnchor),
        ])
    }
    
    public func setCollectionViewCell(with viewModel: CharacterCollectionViewCellViewModel) {
        nameLabel.text = viewModel.name
        viewModel.loadImage { [weak self] result in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    let img = UIImage(data: data)
                    self?.imgView.image = img
                }
            case .failure(let error):
                break
            }
        }
    }
}
