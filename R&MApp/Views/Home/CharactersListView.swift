//
//  CharactersListView.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 29/07/2023.
//

import UIKit

protocol CharactersListViewDelegate: AnyObject {
    func charactersListView(_ charactersListView: CharactersListView, showCharacter character: Character)
}

/// View for show and handler list of characters
class CharactersListView: UIView {
    
    public weak var delegate: CharactersListViewDelegate?
    
    private let viewModel = CharactersListViewViewModel()
    
    /// Spinner for activity indicator on view
    private let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.hidesWhenStopped = true
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
    
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        /// Set Layout
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        /// Set CollectionView
        collectionView.isHidden = true
        collectionView.alpha = 0
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(
            CharacterCollectionViewCell.self,
            forCellWithReuseIdentifier: CharacterCollectionViewCell.cellId
        )
        collectionView.register(
            LoadMoreFooterCollectionReusableView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: LoadMoreFooterCollectionReusableView.id
        )
        
        return collectionView
    }()
    
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(collectionView)
        addSubview(spinner)
        
        setConstraints()
        spinner.startAnimating()
        viewModel.delegate = self
        viewModel.getCharacters()
        setCollectionView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            spinner.widthAnchor.constraint(equalToConstant: 100),
            spinner.heightAnchor.constraint(equalToConstant: 100),
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor)
        ])
    }
    
    private func setCollectionView() {
        collectionView.dataSource = viewModel
        collectionView.delegate = viewModel
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
            self.spinner.stopAnimating()
            
            self.collectionView.isHidden = false
            
            UIView.animate(withDuration: 0.5) {
                self.collectionView.alpha = 1
            }
        })
    }
    
}

extension CharactersListView: CharactersListViewViewModelDelegate {
    
    func loadedCharacters() {
        spinner.stopAnimating()
        
        collectionView.isHidden = false
        collectionView.reloadData()
        
        UIView.animate(withDuration: 0.5) {
            self.collectionView.alpha = 1
        }
    }
    
    func LoadedMoreCharacters(with indexPaths: [IndexPath]) {
        collectionView.performBatchUpdates {
            self.collectionView.insertItems(at: indexPaths)
        }
    }
    
    func showCharacter(_ character: Character) {
        delegate?.charactersListView(self, showCharacter: character)
    }
}
