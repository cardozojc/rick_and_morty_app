//
//  LoadMoreFooterCollectionReusableView.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 30/07/2023.
//

import UIKit

final class LoadMoreFooterCollectionReusableView: UICollectionReusableView {
    static let id = "LoadMoreFooterCollectionReusableView"
    
    /// Spinner for activity indicator on view
    private let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.hidesWhenStopped = true
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemBackground
        addSubview(spinner)
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            spinner.widthAnchor.constraint(equalToConstant: 120),
            spinner.heightAnchor.constraint(equalToConstant: 120),
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
    
    public func startSpinner() {
        spinner.startAnimating()
    }
}
