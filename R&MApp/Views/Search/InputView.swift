//
//  InputView.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 03/08/2023.
//

import UIKit

protocol InputViewDelegate:AnyObject {
    func inputView(_ inputView: InputView, didSelectFilter filter: InputViewViewModel.FilterOptions)
    
    func inputView(_ inputView: InputView, searchTextDidChange text: String)
    
    func inputViewClickedSearchKeyboardBtn(_ inputView: InputView)
}

/// View container for search bar on SearchView
final class InputView: UIView {
    
    weak var delegate: InputViewDelegate?

    private let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.placeholder = "Search"
        return searchBar
    }()
    
    private var viewModel: InputViewViewModel? {
        didSet {
            guard let viewModel = viewModel else {
                return
            }
            let filters = viewModel.filters
            createrSetOfFilters(filters: filters)
        }
    }
    
    private var stackView: UIStackView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }

    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    private func setUpView() {
        backgroundColor = .label.withAlphaComponent(0.1)
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(searchBar)
        setConstraints()
        configure()
        searchBar.delegate = self
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            searchBar.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            searchBar.rightAnchor.constraint(equalTo: rightAnchor, constant: -5),
            searchBar.heightAnchor.constraint(equalToConstant: 55)
        ])
    }
    
    private func configure() {
        self.viewModel = InputViewViewModel()
    }
    
    private func addStackView() -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 5
        addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            stackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -5),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])

        return stackView
    }
    
    private func createrSetOfFilters(filters: [InputViewViewModel.FilterOptions]) {
        let stackView = addStackView()
        self.stackView = stackView
    
        for index in 0 ..< filters.count {
            let option = filters[index]
            let button = addButton(with: option, tag: index)
            stackView.addArrangedSubview(button)
        }
    }
    
    private func addButton(with filter: InputViewViewModel.FilterOptions, tag: Int) -> UIButton {
        let button = UIButton()
        button.setBtnTitle(value: filter.rawValue, color: UIColor.label)
        button.backgroundColor = .secondarySystemFill
        button.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
        button.tag = tag
        button.layer.cornerRadius = 8

        return button
    }
    
    @objc
    private func didTapButton(_ sender: UIButton) {
        guard let filters = viewModel?.filters else {
            return
        }
        
        let tag = sender.tag
        let filterSelected = filters[tag]
        delegate?.inputView(self, didSelectFilter: filterSelected)
        
        #if DEBUG
        print("Filter  --> \(filterSelected.rawValue)")
        #endif
    }
    
    public func updateFilterSelected(filter: InputViewViewModel.FilterOptions, value: String) {
        guard let filtersBtns = stackView?.arrangedSubviews as? [UIButton],
              let filters = viewModel?.filters,
              let indexBtn = filters.firstIndex(of: filter) else {
            return
        }
            
        filtersBtns[indexBtn].setBtnTitle(value: value, color: UIColor.link)
    }
}

extension InputView: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        /// Run when Search Text Change
        delegate?.inputView(self, searchTextDidChange: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        delegate?.inputViewClickedSearchKeyboardBtn(self)
    }
}

extension UIButton {
    
    func setBtnTitle(value: String, color: UIColor) {
        self.setAttributedTitle(
            NSAttributedString(
                string: value.capitalized,
                attributes: [
                    .font: UIFont.systemFont(ofSize: 16, weight: .bold),
                    .foregroundColor: color
                ]
            ),
            for: .normal
        )
    }
    
}

