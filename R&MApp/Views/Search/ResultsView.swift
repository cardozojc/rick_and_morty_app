//
//  ResultsView.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 08/08/2023.
//

import UIKit

protocol ResultsViewDelegate: AnyObject {
    func resultsView(_ resultsView: ResultsView, didSelectCharacter character: Character)
}

/// Shows search results UI (table or collection as needed)
final class ResultsView: UIView {

    weak var delegate: ResultsViewDelegate?
    
    private var viewModel: ResultsViewViewModel? {
        didSet {
            self.processViewModel()
        }
    }

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(CharacterCollectionViewCell.self,
                                forCellWithReuseIdentifier: CharacterCollectionViewCell.cellId)

        // Footer for loading
        collectionView.register(LoadMoreFooterCollectionReusableView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                withReuseIdentifier: LoadMoreFooterCollectionReusableView.id)
        return collectionView
    }()

    /// CollectionView ViewModels
    private var collectionViewCellViewModels: [any Hashable] = []

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        isHidden = true
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(collectionView)
        addConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    private func processViewModel() {
        guard let viewModel = viewModel else {
            return
        }
        
        self.collectionViewCellViewModels = viewModel.results
        setUpCollectionView()
    }

    private func setUpCollectionView() {
        self.collectionView.isHidden = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }


    private func addConstraints() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }

    public func configure(with viewModel: ResultsViewViewModel) {
        self.viewModel = viewModel
    }
}


// MARK: - CollectionView

extension ResultsView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewCellViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let currentViewModel = collectionViewCellViewModels[indexPath.row]
        
        // Model
        guard let characterVM = currentViewModel as? CharacterCollectionViewCellViewModel else {
            fatalError()
        }

        // Character cell
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CharacterCollectionViewCell.cellId,
            for: indexPath
        ) as? CharacterCollectionViewCell else {
            fatalError()
        }

        cell.setCollectionViewCell(with: characterVM)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let currentViewModel = collectionViewCellViewModels[indexPath.row]

        let bounds = collectionView.bounds

        if currentViewModel is CharacterCollectionViewCellViewModel {
            // Character size
            let width = (bounds.width-30)/2
            return CGSize(width: width,height: width)
        }

        // Episode
        let width = bounds.width-20
        return CGSize(
            width: width,
            height: 100
        )
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard kind == UICollectionView.elementKindSectionFooter,
              let footer = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: LoadMoreFooterCollectionReusableView.id,
                for: indexPath
              ) as? LoadMoreFooterCollectionReusableView else {
            fatalError("Unsupported")
        }
        if let viewModel = viewModel, viewModel.shouldShowLoadMoreIndicator {
            footer.startSpinner()
        }
        return footer
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        guard let viewModel = viewModel, let character = viewModel.getSelectedCharacter(at: indexPath.row) else {
            return
        }
        
        #if DEBUG
        print("Character Selected: \(viewModel.results[indexPath.row].name)")
        #endif
        
        delegate?.resultsView(self, didSelectCharacter: character)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        guard let viewModel = viewModel,
              viewModel.shouldShowLoadMoreIndicator else {
            return .zero
        }

        return CGSize(width: collectionView.frame.width, height: 100)
    }
    
}

// MARK: - ScrollViewDelegate
extension ResultsView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        handleCharacterPagination(scrollView: scrollView)
    }

    private func handleCharacterPagination(scrollView: UIScrollView) {
        guard let viewModel = viewModel,
              !collectionViewCellViewModels.isEmpty,
              viewModel.shouldShowLoadMoreIndicator,
              !viewModel.isLoadingMoreResults else {
            return
        }

        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { [weak self] t in
            let offset = scrollView.contentOffset.y
            let totalContentHeight = scrollView.contentSize.height
            let totalScrollViewFixedHeight = scrollView.frame.size.height

            if offset >= (totalContentHeight - totalScrollViewFixedHeight - 120) {
                viewModel.getMoreResults { [weak self] newResults in
                    guard let strongSelf = self else {
                        return
                    }

                    DispatchQueue.main.async {
                        let originalCount = strongSelf.collectionViewCellViewModels.count
                        let newCount = (newResults.count - originalCount)
                        let total = originalCount + newCount
                        let startingIndex = total - newCount
                        let indexPathsToAdd: [IndexPath] = Array(startingIndex..<(startingIndex+newCount)).compactMap({
                            return IndexPath(row: $0, section: 0)
                        })
                        strongSelf.collectionViewCellViewModels = newResults
                        strongSelf.collectionView.insertItems(at: indexPathsToAdd)
                    }
                }
            }
            t.invalidate()
        }
    }
}
