//
//  SearchView.swift
//  R&MApp
//
//  Created by Juan Carlos Cardozo on 02/08/2023.
//

import UIKit

protocol SearchViewDelegate:AnyObject {
    func searchView(_ searchView: SearchView, didSelectFilter filter: InputViewViewModel.FilterOptions)
    func searchView(_ searchView: SearchView, didSelectCharacter character: Character)
}

final class SearchView: UIView {
    
    weak var delegate: SearchViewDelegate?
    private let viewModel: SearchViewViewModel
    
    /// Sub Views
    /// No Results View
    private let noResultsView = NoResultsView()
    
    /// Results View
    private let resultsView = ResultsView()
    
    /// Input Data View
    private let searchParametersView = InputView()
    
    init(frame: CGRect, viewModel: SearchViewViewModel) {
        self.viewModel = viewModel
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Unsupported")
    }
    
    private func setUpView() {
        backgroundColor = .systemBackground
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(resultsView)
        addSubview(noResultsView)
        addSubview(searchParametersView)
        setConstraints()
        
        searchParametersView.delegate = self
        resultsView.delegate = self
        
        viewModel.detectedFilterChange { data in
            self.searchParametersView.updateFilterSelected(filter: data.0, value: data.1)
        }
        
        viewModel.showSearchResults { [weak self] result in
            DispatchQueue.main.async {
                self?.resultsView.configure(with: result)
                self?.noResultsView.isHidden = true
                self?.resultsView.isHidden = false
            }
        }
        
        viewModel.showNoResults { [weak self] in
            DispatchQueue.main.async {
                self?.noResultsView.isHidden = false
                self?.resultsView.isHidden = true
            }
        }
    }
    
    private func setConstraints() {
        resultsView.backgroundColor = .systemPink
        NSLayoutConstraint.activate([
            searchParametersView.topAnchor.constraint(equalTo: topAnchor),
            searchParametersView.leftAnchor.constraint(equalTo: leftAnchor),
            searchParametersView.rightAnchor.constraint(equalTo: rightAnchor),
            searchParametersView.heightAnchor.constraint(equalToConstant: 120),
            
            resultsView.topAnchor.constraint(equalTo: searchParametersView.bottomAnchor),
            resultsView.leftAnchor.constraint(equalTo: leftAnchor),
            resultsView.rightAnchor.constraint(equalTo: rightAnchor),
            resultsView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            noResultsView.widthAnchor.constraint(equalToConstant: 200),
            noResultsView.heightAnchor.constraint(equalToConstant: 200),
            noResultsView.centerXAnchor.constraint(equalTo: centerXAnchor),
            noResultsView.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
    
}

extension SearchView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
}

extension SearchView: InputViewDelegate {

    func inputView(_ inputView: InputView, didSelectFilter filter: InputViewViewModel.FilterOptions) {
        delegate?.searchView(self, didSelectFilter: filter)
    }
    
    func inputView(_ inputView: InputView, searchTextDidChange text: String) {
        viewModel.setInputSearchText(query: text)
    }

    func inputViewClickedSearchKeyboardBtn(_ inputView: InputView) {
        viewModel.searchData()
    }
}

extension SearchView: ResultsViewDelegate  {
    func resultsView(_ resultsView: ResultsView, didSelectCharacter character: Character) {
        delegate?.searchView(self, didSelectCharacter: character)
    }
}
